module.exports = function(grunt) {
        
    grunt.initConfig({
        
        dirName: {
            dev: "builds/dev",
            live: "builds/live"
        },
        
        clean: { 
            dev: '<%= dirName.dev %>/**/*',
            live: '<%= dirName.live %>/**/*'
        },
        
        copy: {
            dev: {
                files: [
                    { '<%= dirName.dev %>/css/vendor/bootstrap.css': 'bower_components/bootstrap/dist/css/bootstrap.css' },
                    { '<%= dirName.dev %>/data/json/photos.json': 'data/json/photosDev.json'},
                    {
                        cwd: 'data/photos/dev',
                        src: '*',
                        dest: '<%= dirName.dev %>/data/photos',
                        expand: true
                    },
                    
                    { '<%= dirName.dev %>/index.html': 'index.html' },
                    { 
                        cwd: 'css/',
                        src: ['**/*', '!vendor/**'],
                        dest: '<%= dirName.dev %>/css',
                        expand: true
                    },
                    { 
                        cwd: 'js/',
                        src: '**/*',
                        dest: '<%= dirName.dev %>/js',
                        expand: true
                    },
                    { '<%= dirName.dev %>/js/vendor/angular.js': 'bower_components/angular/angular.js' },
                    { 
                        cwd: 'img',
                        src: ['**/*', '!pdn/**'],
                        dest: '<%= dirName.dev %>/img',
                        expand: true
                    }
                ]
            },
            
            live: {
                files: [
                    { '<%= dirName.live %>/css/vendor/bootstrap.css': 'bower_components/bootstrap/dist/css/bootstrap.min.css' },
                    { '<%= dirName.live %>/data/json/photos.json': 'data/json/photosLive.json'},
                    {
                        cwd: 'data/photos/live',
                        src: '*',
                        dest: '<%= dirName.live %>/data/photos',
                        expand: true
                    },
                    
                    { '<%= dirName.live %>/index.html': 'index.html' }, 
                    
                    { 
                        cwd: 'css/',
                        src: ['**/*', '!vendor/**'],
                        dest: '<%= dirName.live %>/css',
                        expand: true
                    },
                    
                    { 
                        cwd: 'js/',
                        src: '**/*',
                        dest: '<%= dirName.live %>/js',
                        expand: true
                    },
                    { '<%= dirName.live %>/js/vendor/angular.js': 'bower_components/angular/angular.min.js' }, 
                    { 
                        cwd: 'img',
                        src: ['**/*', '!pdn/**'],
                        dest: '<%= dirName.live %>/img',
                        expand: true
                    }
                ]
            }
        },
        
        watch: {
            dev: {
                files: ['**/*', '!builds/**'],
                tasks: ['cleancopy-dev']
            },
            live: {
                files: ['**/*', '!builds/**'],
                tasks: ['cleancopy-live']
            },  
        }
    });
   
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    
    grunt.registerTask('default', ['copy:dev']);
    grunt.registerTask('live',    ['copy:live']);
    
    grunt.registerTask('cleancopy-dev',  ['clean:dev', 'copy:dev']);
    grunt.registerTask('cleancopy-live', ['clean:live', 'copy:live']);
    grunt.registerTask('clean-dev',      ['clean:dev']);
    grunt.registerTask('clean-live',     ['clean:live']);
    
    grunt.registerTask('watch-dev', ['watch:dev']);
    grunt.registerTask('watch-live', ['watch:live']);
 };