//// getPhotos.
// A service to retrieve and return photos information from the specified JSON file.

app.factory('getPhotos', ['$http', function($http) {
	return $http.get('data/json/photos.json')
		.success(function(data) { return (data); })
		.error(function(err) { return []; });
}]);