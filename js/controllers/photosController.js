//// js/photosController.js
// Stores data and interaction for displaying and changing photos/captions.

app.controller('photosController', ['$scope', 'getPhotos', function($scope, getPhotos) {
	var photosData = {};
	var photosIdx = -1;
	
	$scope.photoCurrent = {};
	
	$scope.navText = {
		prev: "<< PREV",
		next: "NEXT >>"
	};
	
	getPhotos.success(function(data) {
		photosData = data;
		photosIdx = 0;
		updatePhoto();
	});
	
	// Helper functions
	
	var updatePhoto = function() {
		$scope.photoCurrent = {
			"src": photosData.photos[photosIdx].photo,
			"caption": photosData.photos[photosIdx].caption
		};
		
		updateNavText(photosIdx);
	};
	
	var updateNavText = function(photoIdx) {
		var prevText = "";
		var nextText = "";
		
		if (photoIdx === 0)
		{
			prevText = "<< END";
			nextText = "NEXT >>";
		}
		else if (photoIdx === (photosData.photos.length - 1))
		{ 
			prevText = "<< PREV";
			nextText = "START >>";
		}
		else
		{
			prevText = "<< PREV";
			nextText = "NEXT >>";
		}
		
		$scope.navText.prev = prevText;
		$scope.navText.next = nextText;
	}
	
	// Event handlers
	
	$scope.navPrev = function() {
		if (photosIdx === 0) {
			photosIdx = photosData.photos.length - 1;
		}
		else {
			photosIdx = photosIdx - 1;
		}
		
		updatePhoto();
	};
	
	$scope.navNext = function() {
		if (photosIdx === photosData.photos.length - 1) {
			photosIdx = 0;
		}
		else {
			photosIdx = photosIdx + 1;
		}
		
		updatePhoto();
	};
	
	$scope.handleKeydown = function(keycodeEv) {
		var keyCode = keycodeEv.which || keycodeEv.keyCode;
		if (keyCode == 37) // Left
		{
			$scope.navPrev();
		}
		else if (keyCode == 39) // right
		{
			$scope.navNext();
		}
	};
}]);